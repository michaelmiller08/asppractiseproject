﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspcoreproject.Models;
using aspcoreproject.Services;
using Microsoft.AspNetCore.Mvc;
using SQLitePCL;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace aspcoreproject.Controllers
{
    [Route("api/products")]
    public class ProductController : Controller
    {
        public ProductController(IProductService productService)
        {
            ProductService = productService;
        }

        public IProductService ProductService { get; }

        // GET: api/<controller>
        [HttpGet]
        public List<Product> Get()
        {
            return ProductService.GetProducts();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post(Product product)
        {
            ProductService.CreateProduct(product);
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            ProductService.DeleteProduct(id);
        }
    }
}
