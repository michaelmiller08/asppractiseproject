﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace aspcoreproject.Pages
{
    public class AddProductModel : PageModel
    {
        private readonly ILogger<AddProductModel> _logger;

        public AddProductModel(ILogger<AddProductModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}
