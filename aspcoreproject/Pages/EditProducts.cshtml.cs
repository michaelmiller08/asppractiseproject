using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspcoreproject.Models;
using aspcoreproject.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace aspcoreproject.Pages
{
    public class EditProductsModel : PageModel
    {
        private readonly ILogger<EditProductsModel> _logger;
        public readonly IProductService ProductService;
        public List<Product> Products { get; private set; }


        public EditProductsModel(ILogger<EditProductsModel> logger, IProductService productService)
        {
            _logger = logger;
            ProductService = productService;
        }

        public void OnGet()
        {
            Products = ProductService.GetProducts();
        }
    }
}
