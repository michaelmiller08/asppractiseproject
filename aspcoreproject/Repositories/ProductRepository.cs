﻿using aspcoreproject.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using aspcoreproject.DataAccess;

namespace aspcoreproject.Repositories
{
    public class ProductRepository : IProductRepository
    {
        //List<Product> _products = new List<Product>();
        private ApplicationDbContext _dbContext;

        public ProductRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
            //if (_dbContext.Products.ToList().Count == 0)
            //{
            //    var product = new Product
            //    {
            //        Title = "IPhone X",
            //        Description = "iPhone X is the future of the smartphone in a gorgeous all-glass design with a beautiful 5.8-inch Super Retina display. ... The device is the display on iPhone X, featuring the first OLED screen that rises to the standards of iPhone. “For more than a decade, our intention has been to create an iPhone that is all display",
            //        Author = "Michael",
            //        Image = "https://cnet1.cbsistatic.com/img/dVU8S_PYDcCJOnpNDXV5lTQp49o=/1200x630/2017/10/31/312b3b6e-59b7-499a-aea4-9bc5f9721a21/iphone-x-54.jpg",
            //        Price = 550
            //    };
            //    CreateProduct(product);
            //}
        }

        public async void CreateProduct(Product product)
        {
            if (_dbContext.Products.All(p => p.Id != product.Id))
            {
                await _dbContext.Products.AddAsync(product);
                await _dbContext.SaveChangesAsync();
            }
        }

        public List<Product> GetProducts()
        {
            return _dbContext.Products.ToList();
        }

        public async void DeleteProduct(int id)
        {
            _dbContext.Products.Remove(_dbContext.Products.Find(id));
            await _dbContext.SaveChangesAsync();
            //add logging to say product successfully removed
        }

        public void EditProduct(int id)
        {
            //var oldProduct = _dbContext.
            var product = new Product();

            _dbContext.Products.Update(product);
        }

    }
}
