﻿using aspcoreproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspcoreproject.Repositories
{
    public interface IProductRepository
    {
        public void CreateProduct(Product product);

        public List<Product> GetProducts();

        public void DeleteProduct(int id);

        public void EditProduct(int id);
    }
}
