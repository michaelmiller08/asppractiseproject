﻿using aspcoreproject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace aspcoreproject.Services
{
    public interface IProductService
    {
        public void CreateProduct(Product product);

        public List<Product> GetProducts();

        public void DeleteProduct(int id);

        public void EditProduct(int id);
    }
}
