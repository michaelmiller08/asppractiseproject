﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspcoreproject.Models;
using Microsoft.EntityFrameworkCore;

namespace aspcoreproject.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
                
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=./ProductsDB.db");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<Product> Products { get; set; }
    }
}
